#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <gnu/libc-version.h>


int main() {
    printf("pid = %d\nglibc version = %s\n", getpid(), gnu_get_libc_version());

    char input[1] = {0};
    void *m1 = malloc(0x500);
    void *m3 = malloc(0x50);
    free(m1);

    *(long*)m1 = 0x12345678;

	write(1, "done\n", 5);
	read(0, input, 1);
}
