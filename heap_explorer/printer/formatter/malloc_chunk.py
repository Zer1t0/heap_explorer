from heap_explorer.utils import *
from .basic import BasicFormatter


class MallocChunkFormatter(BasicFormatter):

    def __init__(self, pointer_size):
        super(MallocChunkFormatter, self).__init__()
        if pointer_size == 8:
            self._p = p64
        else:
            self._p = p32

    def format_chunk_flags_as_str(self, chunk):
        flags = []
        if chunk.non_main_arena:
            flags.append("NON_MAIN_ARENA")
        if chunk.mmapped:
            flags.append("MMAPPED")
        if chunk.prev_in_use:
            flags.append("PREV_IN_USE")

        return "|".join(flags)

    def format_chunk_first_bytes_as_hexdump_str(self, chunk):
        msg = ""
        raw_bytes = bytearray(self._p(chunk.fd))
        raw_bytes += bytearray(self._p(chunk.bk))

        for byte in raw_bytes:
            msg += "{:02x} ".format(byte)

        msg += "  "

        for byte in raw_bytes:
            msg += chr(byte) if 0x20 <= byte < 0x7F else "."

        return msg