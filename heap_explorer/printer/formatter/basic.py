import shutil


def get_terminal_width():
    try:
        return shutil.get_terminal_size().columns
    except AttributeError:
        import os
        _, columns = os.popen('stty size', 'r').read().split()
        return columns


class BasicFormatter(object):
    _MAXIMUM_WIDTH = 80

    def __init__(self):
        self.width = self._MAXIMUM_WIDTH
        term_width = get_terminal_width()

        if term_width < self.width:
            self.width = term_width

        self.title_surrounded_symbol = "="

    def _format_super_title(self, title):
        msg = "{}\n".format(self._format_title(title, separator="+"))
        msg += "+" * self.width
        return msg

    def _format_title(self, title, separator="="):
        side_len = self._calc_side_len(title)
        side = separator * side_len
        return "{} {} {}".format(side, title, side)

    def _calc_side_len(self, title):
        return int((self.width - len(title) - 2) / 2)

    def _format_close(self):
        return "=" * self.width

    def _format_super_close(self):
        return "+" * self.width
