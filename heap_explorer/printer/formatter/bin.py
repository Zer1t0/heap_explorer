
from .malloc_chunk import MallocChunkFormatter
from heap_explorer.bins import *
from .basic import BasicFormatter


class BinFormatter(BasicFormatter):

    def __init__(self, pointer_size):
        super(BinFormatter, self).__init__()
        self._malloc_chunk_formatter = MallocChunkFormatter(pointer_size)

    def format_bins_summary(self, bins, start_index=0):
        bins_str = []
        for i, bin_ in enumerate(bins):
            if len(bin_) > 0:
                bins_str.append(
                    "    [{}] {:#x} ({})".format(
                        start_index + i, bin_.chunks_size, len(bin_))
                )

        if bins_str:
            return "\n".join(bins_str)
        else:
            return "    [-] No chunks found"

    def format_bins(self, bins, start_index=0, print_all=True):
        bins_str = []
        for i, bin_ in enumerate(bins):
            if print_all or len(bin_) > 0:
                bins_str.append("[{}] {}".format(
                    i + start_index, self._format_bin_as_str(bin_))
                )

        if bins_str:
            return "\n".join(bins_str)
        else:
            return "    [-] No chunks found"

    def _format_bin_as_str(self, bin_):
        msg = self._format_bin_name_as_str(bin_)

        if not isinstance(bin_, UnsortedBin):
            msg += " {:#x}".format(bin_.chunks_size)

        msg += " ({})".format(len(bin_.malloc_chunks))

        next_address = bin_.fd
        for chunk in bin_.malloc_chunks:
            flags = self._malloc_chunk_formatter.format_chunk_flags_as_str(chunk)
            msg += " => Chunk({:#x} {:#x}".format(next_address, chunk.size)
            if flags:
                msg += " {}".format(flags)
            msg += ")"
            next_address = chunk.fd

        msg += " => {:#x}".format(next_address)
        return msg

    def _format_bin_name_as_str(self, bin_):
        if isinstance(bin_, FastBin):
            return "Fastbin"
        elif isinstance(bin_, UnsortedBin):
            return "Unsorted bins"
        elif isinstance(bin_, SmallBin):
            return "Smallbin"
        elif isinstance(bin_, LargeBin):
            return "Largebin"
        elif isinstance(bin_, Tcache):
            return "Tcache"
        else:
            raise TypeError()
