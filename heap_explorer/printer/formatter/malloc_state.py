
from .basic import BasicFormatter


class MallocStateFormatter(BasicFormatter):

    def __init__(self):
        super(MallocStateFormatter, self).__init__()

    def format_malloc_state(self, malloc_state):
        msg = self._format_title(
            "Malloc State ({:#x})".format(malloc_state.address)
        ) + "\n"
        msg += self._format_malloc_state(malloc_state) + "\n"
        msg += self._format_close()
        return msg

    def _format_malloc_state(self, malloc_state):
        string = ""
        string += "mutex = {:#x}\n".format(malloc_state.mutex)
        string += "flags = {:#x}\n".format(malloc_state.flags)

        if malloc_state.have_fastchunks is not None:
            string += "have_fastchunks = {:#x}\n".format(malloc_state.have_fastchunks)

        string += self._format_malloc_state_fastbinsY_as_str(malloc_state.fastbinsY)

        string += "top = {:#x}\n".format(malloc_state.top)
        string += "last_remainder = {:#x}\n".format(malloc_state.last_remainder)
        string += self._format_malloc_state_bins_as_str(malloc_state.bins)

        string += "binmap = [{:#x}, {:#x}, {:#x}, {:#x}]\n".format(
            malloc_state.binmap[0],
            malloc_state.binmap[1],
            malloc_state.binmap[2],
            malloc_state.binmap[3]
        )
        string += "next = {:#x}\n".format(malloc_state.next)
        string += "next_free = {:#x}\n".format(malloc_state.next_free)

        if malloc_state.attached_threads is not None:
            string += "attached_threads = {:#x}\n".format(
                malloc_state.attached_threads
            )
        string += "system_mem = {:#x}\n".format(malloc_state.system_mem)
        string += "max_system_mem = {:#x}".format(malloc_state.max_system_mem)

        return string

    def _format_malloc_state_fastbinsY_as_str(self, fastbinsY):
        string = "fastbinsY\n"
        for i, entry in enumerate(fastbinsY):
            string += "  [{}] {:#x} => {:#x}\n".format(
                i, entry.chunks_size, entry.fd)

        return string

    def _format_malloc_state_bins_as_str(self, bins):
        string = "bins\n"
        index = 0

        string += " Unsorted bins\n"
        unsorted_entry = bins.unsorted_bin_entry
        string += "  [{}] fd={:#x} bk={:#x}\n".format(
            index, unsorted_entry.fd, unsorted_entry.bk)

        index += 1
        string += " Small bins\n"
        for small_entry in bins.small_bins_entries:
            string += "  [{}] {:#x} fd={:#x} bk={:#x}\n".format(
                index, small_entry.chunks_size, small_entry.fd, small_entry.bk)
            index += 1

        string += " Large bins\n"
        for small_entry in bins.large_bins_entries:
            string += "  [{}] {:#x} fd={:#x} bk={:#x}\n".format(
                index, small_entry.chunks_size, small_entry.fd, small_entry.bk)
            index += 1

        return string
