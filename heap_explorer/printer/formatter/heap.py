from .malloc_chunk import *
from .basic import BasicFormatter


class HeapFormatter(BasicFormatter):

    def __init__(self, pointer_size):
        super(HeapFormatter, self).__init__()
        self._malloc_chunk_formatter = MallocChunkFormatter(pointer_size)

    def format_heap(self, heap):
        msg = [
            self._format_title("Heap ({:#x})".format(heap.address)),
            self._format_heap(heap),
            self._format_close()
        ]
        return "\n".join(msg)

    def _format_heap(self, heap):
        chunks_str = [self._format_heap_chunk_as_str(chunk) for chunk in heap.chunks]
        return "\n".join(chunks_str)

    def _format_heap_chunk_as_str(self, chunk):
        msg = ""
        flags = self._malloc_chunk_formatter.format_chunk_flags_as_str(chunk)

        msg += "{:#x} {:#x} {}".format(chunk.address, chunk.size, flags)
        msg += "\n"
        msg += "  " + self._malloc_chunk_formatter.format_chunk_first_bytes_as_hexdump_str(chunk)

        return msg

