from heap_explorer.malloc_state import *
from heap_explorer.bins import *
from .bin import BinFormatter
from .basic import BasicFormatter


class ArenaFormatter(BasicFormatter):

    def __init__(self, pointer_size):
        super(ArenaFormatter, self).__init__()
        self._bins_formatter = BinFormatter(pointer_size)

    def format_arena_summary(self, arena):
        msg = ""

        malloc_state = arena.malloc_state
        msg += "- Malloc State ({:#x})\n".format(malloc_state.address)
        msg += "    top = {:#x}\n".format(malloc_state.top)
        msg += "    last_remainder = {:#x}\n".format(malloc_state.last_remainder)
        msg += "    next = {:#x}\n".format(malloc_state.next)
        msg += "    next_free = {:#x}\n".format(malloc_state.next_free)
        msg += "    system_mem = {:#x}\n".format(malloc_state.system_mem)

        heap = arena.heap
        msg += "- Heap ({:#x})\n".format(heap.address)
        msg += "    chunks_count = {:#x}\n".format(len(heap.chunks))
        msg += "    top: addr = {:#x}, size = {:#x}\n".format(heap.top.address, heap.top.size)

        try:
            tcaches = arena.tcaches
            msg += "- Tcaches\n"
            msg += "{}\n".format(
                self._bins_formatter.format_bins_summary(tcaches)
            )
        except NoTcacheError:
            pass

        msg += "- Fast bins\n"
        msg += "{}\n".format(
            self._bins_formatter.format_bins_summary(arena.fast_bins)
        )

        msg += "- Unsorted bins\n"
        if len(arena.unsorted_bin) > 0:
            msg += "    [0] ({})\n".format(len(arena.unsorted_bin))
        else:
            msg += "    [-] No chunks found\n"

        msg += "- Small bins\n"
        msg += "{}\n".format(
            self._bins_formatter.format_bins_summary(
                arena.small_bins,
                start_index=SMALL_BINS_START_INDEX
            )
        )

        msg += "- Large bins\n"
        msg += "{}".format(
            self._bins_formatter.format_bins_summary(
                arena.large_bins,
                start_index=LARGE_BINS_START_INDEX
            )
        )

        return msg
