from pwn import *

from heap_explorer.printer import BinPrinter
from heap_explorer.heap_explorer import HeapExplorer
import sys


# TODO: check if tcache exists (check with libc previous to 2.26)
# TODO: do tests of parsing the malloc_state (and the arena)
#   * libc > 2.26 and 64 bits
#   * libc > 2.26 and 32 bits
#   * libc = 2.26 and 64 bits
#   * libc = 2.26 and 32 bits
#   * libc = 2.25 and 64 bits
#   * libc = 2.25 and 32 bits
#   * libc < 2.25 and 64 bits
#   * libc < 2.25 and 32 bits


# TODO: test it in ubuntu 14.04
# TODO: compile glibc or search for sos with the required libcs

def read_heap_bins():
    p = process([sys.argv[1]])
    p.recv()
    libc = p.libc
    pid = p.pid

    if "64" in libc.get_machine_arch():
        pointer_size = 8
    else:
        pointer_size = 4

    bin_printer = BinPrinter(pointer_size)
    heap_explorer = HeapExplorer(pid, libc)

    print("are tcache enabled? {}".format(heap_explorer.tcaches_enabled))
    # malloc_state = heap_explorer.malloc_state()

    # unsorted_bin = heap_explorer.unsorted_bin()
    # bin_printer.print_unsorted_bin(unsorted_bin)

    # for large_bins in heap_explorer.all_arenas_large_bins():
        # bin_printer.print_large_bins(large_bins)

    # for unsorted_bin in heap_explorer.all_arenas_unsorted_bins():
    #     bin_printer.print_unsorted_bin(unsorted_bin)

    # for small_bins in heap_explorer.all_arenas_small_bins():
    #     bin_printer.print_small_bins(small_bins)

    # bin_printer.print_malloc_state(heap_explorer.malloc_state())

    # for malloc_state in heap_explorer.all_arenas_malloc_states():
    #     print(malloc_state.format_beauty())

    # for arena in heap_explorer.all_arenas():
        # bin_printer.print_arena_summary(arena)

    # bin_printer.print_heap(heap_explorer.heap())
    for heap in heap_explorer.all_arenas_heaps():
        print(heap)

    # for tcaches in heap_explorer.all_arenas_tcaches():
    #     bin_printer.print_tcaches(tcaches)

    # for fastbins in all_fastbins:
    #     bin_printer.print_fast_bins(fastbins)

    # for bins in all_bins:
    #     bin_printer.print_unsorted_bin(bins.unsorted_bin)
    #     bin_printer.print_small_bins(bins.small_bins)
    #     bin_printer.print_large_bins(bins.large_bins)

    input("{}".format(pid))

    p.close()


if __name__ == "__main__":
    # read_heap_chunks()
    read_heap_bins()


